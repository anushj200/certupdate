**Cert Update playbook:**



> Cert Update playbok will add the certs to OS cert store on RHEL and Debian flavours


**Certupload project directory structure:**

```
certupdate:
--> group_vars
    --> cert (variable file)
--> roles
    --> cert
        ---> tasks
        ---> handlers
```


**Steps needs to be perform:**

```
1. Store all the certs on files/ca folder
2. create a group called "cert" on inventory file
3. All all the servers to that inventory group 
4. Run the playbook
```

`ansible-playbook -i inv addcert.yaml`


